module ApplicationHelper
  # タイトルの共通化
  def full_title(page_title)
    base_title = "BIGBAG store"
    if page_title.blank?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end
end
