class Potepan::CategoriesController < ApplicationController
  helper_method :products_count_option

  def show
    @categories = Spree::Taxonomy.all.includes(:root)
    @taxon      = Spree::Taxon.find(params[:id])
    @colors     = Spree::OptionType.find_by(presentation: "Color").option_values
    @sizes      = Spree::OptionType.find_by(presentation: "Size").option_values
    @products   = filtered_products.sort_types(params[:sort])
  end

  private

  def products_count_option(option_value)
    Spree::Product.in_taxon(@taxon).search_option_value(option_value).count
  end

  def filtered_products
    scope = @taxon.all_products.includes_price_and_image
    params[:option_value].presence || scope.search_option_value(params[:option_value])
  end
end
