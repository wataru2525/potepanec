class Potepan::ProductsController < ApplicationController
  MAX_NUMBER_OF_RELATED_PRODUCTS = 4

  def show
    @product          = Spree::Product.find(params[:id])
    @related_products = @product.related_products.includes(master: [:default_price, :images]).limit(MAX_NUMBER_OF_RELATED_PRODUCTS)
    @line_item = Spree::LineItem.new
    if @product.empty_option_values?
      @variant = @product.master
      @value_array = []
    else
      @value_array = @product.variants.map { |val| [val.options_text, "#{val.id}"] }
    end
  end

  def search
    @search = params[:search]
    @products = Spree::Product.includes_price_and_image.search_option(@search)
  end
end
