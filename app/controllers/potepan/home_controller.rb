class Potepan::HomeController < ApplicationController
  MAX_NEW_PRODUCTS_IMAGE = 8

  def index
    @new_products = Spree::Product.includes(master: [:default_price, :images]).order(available_on: "DESC").limit(MAX_NEW_PRODUCTS_IMAGE)
  end
end
