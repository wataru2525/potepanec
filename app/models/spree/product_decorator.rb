module Spree::ProductDecorator
  def self.prepended(base)
    # default_price, imagesを含める
    base.scope :includes_price_and_image, -> { base.includes(master: [:default_price, :images]) }
    # Variant配下のOptionValueを取得して、OptionValueのnameがURLクエリと一致する商品を取得
    base.scope :search_option_value, -> (option_value) { base.includes(variants: :option_values).where(spree_option_values: { name: option_value }) }
    # 商品の並び替え
    base.scope :new_products, -> { base.order(available_on: "desc") }
    base.scope :old_products, -> { base.order(available_on: "asc") }
    base.scope :sort_types, -> (sort) {
      case sort
      when "NEW_PRODUCTS"
        reorder(nil).new_products
      when "OLD_PRODUCTS"
        reorder(nil).old_products
      when "LOW_PRICE"
        unscope(:order).ascend_by_master_price
      when "HIGH_PRICE"
        unscope(:order).descend_by_master_price
      end
    }
    # 検索機能
    base.scope :search_option, -> (search_word) { where('name LIKE ? OR description LIKE ?', "%#{sanitize_sql_like(search_word)}%", "%#{sanitize_sql_like(search_word)}%") if search_word.present? }
  end

  # 関連商品の取得（distinct→product自身を関連商品として表示しない）
  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct
  end

  Spree::Product.prepend self
end
