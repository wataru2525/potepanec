require 'rails_helper'

RSpec.describe Potepan::OrdersController, type: :controller do
  let!(:store) { create(:store) }

  describe "GET#edit" do
    let!(:order) { create(:order, guest_token: 'abcde') }

    before do
      cookies.signed[:guest_token] = 'abcde'
      get :edit
    end

    it "editへのアクセスに対して正常なレスポンスが返ってくる" do
      expect(response).to be_successful
    end

    it "editテンプレートが表示される" do
      expect(response).to render_template :edit
    end

    it "@orderが期待された値を持っている" do
      expect(assigns(:order)).to eq order
    end
  end

  describe "PATCH#update" do
    # https://github.com/solidusio/solidus/blob/master/core/lib/spree/testing_support/factories/order_factory.rb
    let!(:order) { create(:order_with_line_items) }
    let!(:line_item) { order.line_items.first }
    let!(:order_params) do
      { order: { line_items_attributes: { quantity: 2, id: line_item.id } }, number: line_item.order.number }
    end

    it "数量が更新される" do
      expect(line_item.quantity).to eq 1
      patch :update, params: order_params
      line_item.reload
      expect(line_item.quantity).to eq 2
    end
  end
end
