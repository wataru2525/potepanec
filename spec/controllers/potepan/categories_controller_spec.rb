require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:color) { create(:option_type, presentation: 'Color') }
    let!(:colors) do
      [
        create(:option_value, name: 'Red', option_type: color),
        create(:option_value, name: 'Bule', option_type: color),
        create(:option_value, name: 'Green', option_type: color),
      ]
    end
    let!(:size) { create(:option_type, presentation: 'Size') }
    let!(:sizes) do
      [
        create(:option_value, name: 'Small', option_type: size),
        create(:option_value, name: 'Medium', option_type: size),
        create(:option_value, name: 'Large', option_type: size),
      ]
    end

    before do
      get :show, params: { id: taxon.id }
    end

    it "showへのアクセスに対して正常なレスポンスが返ってくる" do
      expect(response).to be_successful
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@categoriesが期待された値を持っている" do
      expect(assigns(:categories)).to contain_exactly(taxonomy)
    end

    it "@taxonが期待された値を持っている" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "@productsが期待された値を持っている" do
      expect(assigns(:products)).to contain_exactly(product)
    end

    it "@colorsが期待された値を持っている" do
      expect(assigns(:colors)).to match_array colors
    end

    it "@sizesが期待された値を持っている" do
      expect(assigns(:sizes)).to match_array sizes
    end
  end
end
