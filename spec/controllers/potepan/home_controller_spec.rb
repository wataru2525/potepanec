require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    let!(:new_products) { create(:product, available_on: Time.zone.now) }

    before do
      get :index
    end

    it "indexへのアクセスに対して正常なレスポンスが返ってくる" do
      expect(response).to be_successful
    end

    it "indexテンプレートが表示される" do
      expect(response).to render_template :index
    end

    it "@new_productsが期待された値を持っている" do
      expect(assigns(:new_products)).to match_array(new_products)
    end
  end
end
