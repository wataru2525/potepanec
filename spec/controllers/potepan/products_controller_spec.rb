require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }
    let!(:other_taxon) { create(:taxon) }
    let!(:product2) { create(:product, taxons: [taxon, other_taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before { get :show, params: { id: product.id } }

    it "showへのアクセスに対して正常なレスポンスが返ってくる" do
      expect(response).to be_successful
    end

    it "showテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@productが期待された値を持っている" do
      expect(assigns(:product)).to eq product
    end

    it "@related_productsが期待された値を持っている" do
      expect(assigns(:related_products)).to include product2
    end

    it "４つの関連商品表示される" do
      expect(assigns(:related_products).count).to eq 4
    end

    it "メイン商品が関連商品に含まれない" do
      expect(assigns(:related_products)).not_to include product
    end
  end

  describe "GET #search" do
    let!(:products) do
      [
        create(:product, name: 'RUBY TOTE', description: 'There is a RUBY TOTE'),
        create(:product, name: 'RAILS MAG', description: 'There is a RAILS MUG'),
      ]
    end

    before { get :search, params: { search: 'RUBY TOTE' } }

    it "searchへのアクセスに対して正常なレスポンスが返ってくる" do
      expect(response).to be_successful
    end

    it "searchテンプレートが表示される" do
      expect(response).to render_template :search
    end

    it "@productが期待された値を持っている" do
      expect(assigns(:products)).to include products[0]
      expect(assigns(:products)).not_to include products[1]
    end
  end

  context "検索にメタキャラクタを入力した場合" do
    let!(:metacharacter_product) { create(:product, name: "WOOL100% KNIT") }
    let!(:not_metacharacter_product) { create(:product, name: "KNIT") }

    before { get :search, params: { search: '100%' } }

    it "正常なレスポンスが返ってくる" do
      expect(response).to be_successful
    end

    it "メタキャラクタを持つワードでの検索が成功する" do
      expect(assigns(:products)).to include metacharacter_product
      expect(assigns(:products)).not_to include not_metacharacter_product
    end
  end
end
