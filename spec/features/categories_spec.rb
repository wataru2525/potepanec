require 'rails_helper'

RSpec.feature "categories show pages", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product) }
  let!(:colors) { create(:option_type, presentation: 'Color') }
  let!(:product_color) { create(:option_value, name: 'Red', option_type: colors) }
  let!(:sizes) { create(:option_type, presentation: 'Size') }
  let!(:product_size) { create(:option_value, name: 'Small', option_type: sizes) }
  let!(:taxon_1) { create(:taxon, name: 'Bags', taxonomy: taxonomy) }
  let!(:taxon_2) { create(:taxon, name: 'Socks', taxonomy: taxonomy) }
  let!(:product_1) { create(:product, name: 'Pants', price: 100, available_on: 1.day.ago, taxons: [taxon_1], option_types: [colors]) }
  let!(:product_2) { create(:product, name: 'Tshirt', price: 10, available_on: 2.day.ago, taxons: [taxon_1]) }
  let!(:product_3) { create(:product, name: 'Shoes', price: 50, taxons: [taxon_2], option_types: [sizes]) }

  before do
    visit potepan_category_path(id: taxon.id)
  end

  scenario "カテゴリーページが表示されている" do
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_no_content other_product.name
  end

  scenario "カテゴリーサイドバーが表示されている" do
    within ".sideBar" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content product_color.name
      expect(page).to have_content product_size.presentation
    end
  end

  scenario "商品詳細ページへのリンクが機能している" do
    expect(page).to have_link product.name
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario "カラーのある商品のみが表示される" do
    visit potepan_category_path(id: taxon_1.id)
    expect(page).to have_link product_color.name
    within ".filter-by-color" do
      click_on product_color.name
    end
    within ".productBox", match: :first do
      expect(page).to have_content 'Pants'
      expect(page).not_to have_content 'Tshirt'
    end
  end

  scenario "サイズのある商品のみが表示される" do
    visit potepan_category_path(id: taxon_2.id)
    expect(page).to have_link product_size.presentation
    within ".filter_by_size" do
      click_on product_size.presentation
    end
    within ".productBox" do
      expect(page).to have_content 'Shoes'
      expect(page).not_to have_content 'Pants'
    end
  end

  scenario "並び替えのドロップダウンが表示されている" do
    select("新着順", from: "sort")
    expect(page).to have_select("sort", selected: "新着順")
    select("高い順", from: "sort")
    expect(page).to have_select("sort", selected: "高い順")
    select("安い順", from: "sort")
    expect(page).to have_select("sort", selected: "安い順")
    select("古い順", from: "sort")
    expect(page).to have_select("sort", selected: "古い順")
  end

  scenario "並べ替えが正しく表示されている" do
    expect(Spree::Product.in_taxon(taxon_1).sort_types('NEW_PRODUCTS')).to match [product_1, product_2]
    expect(Spree::Product.in_taxon(taxon_1).sort_types('OLD_PRODUCTS')).to match [product_2, product_1]
    expect(Spree::Product.in_taxon(taxon_1).sort_types('LOW_PRICE')).to    match [product_2, product_1]
    expect(Spree::Product.in_taxon(taxon_1).sort_types('HIGH_PRICE')).to   match [product_1, product_2]
  end
end
