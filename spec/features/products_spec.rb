require 'rails_helper'

RSpec.feature "Products show pages", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:taxon) { create(:taxon) }
  let!(:related_products) { create(:product, taxons: [taxon]) }
  let!(:ruby_product) { create(:product, name: 'Ruby Tote', taxons: [taxon]) }
  let!(:rails_product) { create(:product, name: 'Rails Tote', taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "showページへアクセスした時" do
    expect(page).to have_content product.name
    expect(page).to have_content product.description
    expect(page).to have_content product.display_price
  end

  scenario "showページへアクセスした時(関連商品の表示)" do
    within '.productsContent' do
      expect(page).to have_content related_products.name
      expect(page).to have_content related_products.display_price
    end
  end

  scenario "関連商品からのリンクが機能している" do
    expect(page).to have_link related_products.name
    click_on related_products.name
    expect(current_path).to eq potepan_product_path(related_products.id)
  end

  scenario "searchページへアクセスした時の商品の表示、リンクが機能している" do
    visit search_potepan_products_path(search: 'Ruby Tote')
    within '.productsContent' do
      expect(page).to have_content 'Ruby Tote'
      expect(page).not_to have_content 'Rails Tote'
    end
    expect(page).to have_link 'Ruby Tote'
    click_on 'Ruby Tote'
    expect(current_path).to eq potepan_product_path(ruby_product.id)
  end
end
