require 'rails_helper'

RSpec.feature "order pages", type: :feature do
  let!(:product) { create(:product, name: 'Ruby Mag', taxons: [taxon]) }
  let!(:taxon) { create(:taxon) }
  let!(:store) { create(:store) }

  before do
    visit potepan_product_path(product.id)
    click_on "カートへ入れる"
  end

  scenario "カートページが表示されている、商品が追加されている" do
    within ".cartListInner" do
      expect(page).to have_content 'Ruby Mag'
      expect(current_path).to eq potepan_cart_path
    end
  end

  scenario "カート商品の削除が機能している" do
    within ".cartListInner" do
      click_on "×"
      expect(page).not_to have_content 'Ruby Mag'
      expect(page).to have_content "カートは空です"
    end
  end
end
