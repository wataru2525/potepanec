require 'rails_helper'

RSpec.feature "Products index pages", type: :feature do
  let!(:new_product) { create(:product, taxons: [taxon]) }
  let!(:taxon) { create(:taxon) }

  before do
    visit potepan_path
  end

  scenario "indexページへアクセスした時(新着商品の表示)" do
    within '.featuredProductsSlider' do
      expect(page).to have_content new_product.name
      expect(page).to have_content new_product.display_price
    end
  end

  scenario "新着商品からのリンクが機能している" do
    expect(page).to have_link new_product.name
    click_on new_product.name
    expect(current_path).to eq potepan_product_path(new_product.id)
  end
end
